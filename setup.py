from setuptools import setup

setup(name='CloudBase-OSS-PySDK',
      version='1.1',
      description='CloudBase OSS Python sdk',
      url='http://https://nhanct@bitbucket.org/cloud360/s3-python-sdk.git',
      author='HoangTO',
      author_email='me@example.com',
      license='MIT',
      packages=['s3pysdk'],
      zip_safe=False)

